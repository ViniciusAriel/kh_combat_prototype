// Copyright Epic Games, Inc. All Rights Reserved.

#include "KH_Combat.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, KH_Combat, "KH_Combat" );
