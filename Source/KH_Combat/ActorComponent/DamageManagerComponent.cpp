// Fill out your copyright notice in the Description page of Project Settings.


#include "DamageManagerComponent.h"

// Sets default values for this component's properties
UDamageManagerComponent::UDamageManagerComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = false;
}


// Called when the game starts
void UDamageManagerComponent::BeginPlay()
{
	Super::BeginPlay();

	CurrentHealth = TotalHealth;
}


// Called every frame
void UDamageManagerComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
}

float UDamageManagerComponent::GetTotalHealth()
{
	return TotalHealth;
}

float UDamageManagerComponent::GetCurrentHealth()
{
	return CurrentHealth;
}

void UDamageManagerComponent::ChangeHealth(float HealthOffset)
{
	CurrentHealth += HealthOffset;

	if(CurrentHealth <= 0.f)
	{
		UE_LOG(LogTemp, Warning, TEXT("Actor died!"));
	}
}