#pragma once

#include "CoreMinimal.h"
#include "Engine/DataTable.h"
#include "Engine/TriggerBase.h"
#include "CollisionShape.h"
#include "CombatDataStructures.generated.h"

UENUM(BlueprintType, Category="Combat")
enum class EWeaponTypes : uint8
{
	Sword,
	Spear,
};

UENUM(BlueprintType, Category = "Combat")
enum class ETriggerActorShapes : uint8
{
	Box,
	Capsule,
	Sphere,
};

USTRUCT(BlueprintType)
struct FWeaponAttackInfo : public FTableRowBase
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	UAnimMontage* AnimationMontage;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	ETriggerActorShapes TriggerShape;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FVector ShapeSpawnOffset;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FVector BoxExtent;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FRotator Rotation;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float Radius;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float CapsuleHalfHeight;
};