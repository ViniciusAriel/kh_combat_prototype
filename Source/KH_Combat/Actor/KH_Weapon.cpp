// Fill out your copyright notice in the Description page of Project Settings.


#include "KH_Weapon.h"
#include "Components/StaticMeshComponent.h"
#include "Components/BoxComponent.h"
#include "Engine/DataTable.h"

// Sets default values
AKH_Weapon::AKH_Weapon()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	StaticMeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Static Mesh Component"));
	SetRootComponent(StaticMeshComponent);

	BoxCollisionComponent = CreateDefaultSubobject<UBoxComponent>(TEXT("Box Collision Component"));
	BoxCollisionComponent->SetupAttachment(StaticMeshComponent);
}

// Called when the game starts or when spawned
void AKH_Weapon::BeginPlay()
{
	Super::BeginPlay();
	
	ComboLenght = BaseComboLenght;
	DamageToDeal = BaseDamage;

	if(AttackInfoDataTable)
	{
		AttackInfoDataTable->GetRowMap().GenerateKeyArray(AttackInfoOrderedRowNames);
	}
}

// Called every frame
void AKH_Weapon::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

int32 AKH_Weapon::ChangeComboLenght(int ComboModifier)
{
	ComboLenght = BaseComboLenght + ComboModifier;
	return ComboLenght;
}

void AKH_Weapon::WeaponAttack()
{
	GetWorldTimerManager().SetTimer(ComboResetTimerHandle, this, &AKH_Weapon::ResetComboIndex, ComboResetTimer);
}

int32 AKH_Weapon::IncrementCurrentComboIndex()
{
	CurrentComboIndex++;

	if(CurrentComboIndex >= ComboLenght)
	{
		CurrentComboIndex = 0;
		DamageToDeal = BaseDamage;
	}
	else if(CurrentComboIndex == ComboLenght - 1)
	{
		DamageToDeal = BaseDamage * FinishingBlowDamageMultiplier;
	}

	return CurrentComboIndex;
}

void AKH_Weapon::ResetComboIndex()
{
	CurrentComboIndex = 0;
}