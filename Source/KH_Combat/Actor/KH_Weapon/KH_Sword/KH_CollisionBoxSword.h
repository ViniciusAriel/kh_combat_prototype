// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "../KH_Sword.h"
#include "KH_CollisionBoxSword.generated.h"

/**
 *
 */
UCLASS()
class KH_COMBAT_API AKH_CollisionBoxSword : public AKH_Sword
{
	GENERATED_BODY()

protected:
	virtual void BeginPlay() override;

public:
	virtual void WeaponAttack() override;

protected:
	void SpawnAttackTriggerBox(struct FWeaponAttackInfo* AttackInfo, FVector SpawnLocation, FRotator Rotation);

	void SpawnAttackTriggerCapsule(struct FWeaponAttackInfo* AttackInfo, FVector SpawnLocation, FRotator Rotation);

	void SpawnAttackTriggerSphere(struct FWeaponAttackInfo* AttackInfo, FVector SpawnLocation, FRotator Rotation);

	UFUNCTION()
	virtual void OnTriggerShapeBeginOverlap(class UPrimitiveComponent* OverlappedComp, AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& Hit);

	void CheckOverlappedActors(TArray<AActor*> OverlappedActors);

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Combat Settings", meta=(AllowPrivateAccess="true"))
	float AttackTriggerShapeLifeSpan = 0.5f;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Combat Settings", meta=(AllowPrivateAccess="true"))
	FCollisionProfileName AttackCollisionProfile;
};
