// Fill out your copyright notice in the Description page of Project Settings.


#include "KH_CollisionBoxSword.h"
#include "../../../DataStructures/CombatDataStructures.h"
#include "DrawDebugHelpers.h"
#include "Engine/TriggerBox.h"
#include "Engine/TriggerCapsule.h"
#include "Engine/TriggerSphere.h"
#include "Components/BoxComponent.h"
#include "Components/CapsuleComponent.h"
#include "Components/SphereComponent.h"
#include "Kismet/GameplayStatics.h"

void AKH_CollisionBoxSword::BeginPlay()
{
	Super::BeginPlay();
}

void AKH_CollisionBoxSword::WeaponAttack()
{
	Super::WeaponAttack();

	AActor* OwnerActor = GetOwner();

	if(OwnerActor && AttackInfoDataTable)
	{
		static const FString ContextString(TEXT("AttackInfoDataTable Context: CollisionBoxSword"));
		FWeaponAttackInfo* AttackInfo = AttackInfoDataTable->FindRow<FWeaponAttackInfo>(AttackInfoOrderedRowNames[CurrentComboIndex], ContextString, true);

		if(AttackInfo)
		{
			FVector SpawnLocationForwardOffset = OwnerActor->GetActorForwardVector() * AttackInfo->ShapeSpawnOffset.X;
			FVector SpawnLocationRightOffset = OwnerActor->GetActorRightVector() * AttackInfo->ShapeSpawnOffset.Y;
			FVector SpawnLocationUpOffset = OwnerActor->GetActorUpVector() * AttackInfo->ShapeSpawnOffset.Z;

			FVector TriggerShapeSpawnLocation = OwnerActor->GetActorLocation() + SpawnLocationForwardOffset + SpawnLocationRightOffset + SpawnLocationUpOffset;

			FRotator ShapeRotation = OwnerActor->GetActorRotation() + AttackInfo->Rotation;

			if(AttackInfo->TriggerShape == ETriggerActorShapes::Box)
			{
				SpawnAttackTriggerBox(AttackInfo, TriggerShapeSpawnLocation, ShapeRotation);
			}
			else if(AttackInfo->TriggerShape == ETriggerActorShapes::Capsule)
			{
				SpawnAttackTriggerCapsule(AttackInfo, TriggerShapeSpawnLocation, ShapeRotation);
			}
			else if(AttackInfo->TriggerShape == ETriggerActorShapes::Sphere)
			{
				SpawnAttackTriggerSphere(AttackInfo, TriggerShapeSpawnLocation, ShapeRotation);
			}

			IncrementCurrentComboIndex();
		}
	}
}

void AKH_CollisionBoxSword::SpawnAttackTriggerBox(struct FWeaponAttackInfo* AttackInfo, FVector SpawnLocation, FRotator Rotation)
{
	ATriggerBox* AttackTriggerBox = GetWorld()->SpawnActor<ATriggerBox>(ATriggerBox::StaticClass(), SpawnLocation, Rotation);

	if(AttackTriggerBox)
	{
		AttackTriggerBox->SetInstigator(GetInstigator());
		UBoxComponent* AttackBoxComponent = Cast<UBoxComponent>(AttackTriggerBox->GetCollisionComponent());

		AttackBoxComponent->OnComponentBeginOverlap.AddDynamic(this, &AKH_CollisionBoxSword::OnTriggerShapeBeginOverlap);
		AttackBoxComponent->SetBoxExtent(AttackInfo->BoxExtent, false);
		AttackBoxComponent->SetCollisionProfileName(AttackCollisionProfile.Name, true);

		AttackTriggerBox->SetLifeSpan(AttackTriggerShapeLifeSpan);

		TArray<AActor*> CurrentlyOverlappedActors;
		AttackBoxComponent->GetOverlappingActors(CurrentlyOverlappedActors);

		CheckOverlappedActors(CurrentlyOverlappedActors);

		if(bDebugMode)
		{
			AttackTriggerBox->SetActorHiddenInGame(false);
		}
	}
}

void AKH_CollisionBoxSword::SpawnAttackTriggerCapsule(struct FWeaponAttackInfo* AttackInfo, FVector SpawnLocation, FRotator Rotation)
{
	ATriggerCapsule* AttackTriggerCapsule = GetWorld()->SpawnActor<ATriggerCapsule>(ATriggerCapsule::StaticClass(), SpawnLocation, Rotation);

	if(AttackTriggerCapsule)
	{
		AttackTriggerCapsule->SetInstigator(GetInstigator());
		UCapsuleComponent* AttackCapsuleComponent = Cast<UCapsuleComponent>(AttackTriggerCapsule->GetCollisionComponent());

		AttackCapsuleComponent->OnComponentBeginOverlap.AddDynamic(this, &AKH_CollisionBoxSword::OnTriggerShapeBeginOverlap);
		AttackCapsuleComponent->SetCapsuleSize(AttackInfo->Radius, AttackInfo->CapsuleHalfHeight, false);
		AttackCapsuleComponent->SetCollisionProfileName(AttackCollisionProfile.Name, true);

		AttackTriggerCapsule->SetLifeSpan(AttackTriggerShapeLifeSpan);

		TArray<AActor*> CurrentlyOverlappedActors;
		AttackCapsuleComponent->GetOverlappingActors(CurrentlyOverlappedActors);

		CheckOverlappedActors(CurrentlyOverlappedActors);

		if(bDebugMode)
		{
			AttackTriggerCapsule->SetActorHiddenInGame(false);
		}
	}
}

void AKH_CollisionBoxSword::SpawnAttackTriggerSphere(struct FWeaponAttackInfo* AttackInfo, FVector SpawnLocation, FRotator Rotation)
{
	ATriggerSphere* AttackTriggerSphere = GetWorld()->SpawnActor<ATriggerSphere>(ATriggerSphere::StaticClass(), SpawnLocation, Rotation);

	if(AttackTriggerSphere)
	{
		AttackTriggerSphere->SetInstigator(GetInstigator());
		USphereComponent* AttackSphereComponent = Cast<USphereComponent>(AttackTriggerSphere->GetCollisionComponent());

		AttackSphereComponent->OnComponentBeginOverlap.AddDynamic(this, &AKH_CollisionBoxSword::OnTriggerShapeBeginOverlap);
		AttackSphereComponent->SetSphereRadius(AttackInfo->Radius, false);
		AttackSphereComponent->SetCollisionProfileName(AttackCollisionProfile.Name, true);

		AttackTriggerSphere->SetLifeSpan(AttackTriggerShapeLifeSpan);

		TArray<AActor*> CurrentlyOverlappedActors;
		AttackSphereComponent->GetOverlappingActors(CurrentlyOverlappedActors);

		CheckOverlappedActors(CurrentlyOverlappedActors);

		if(bDebugMode)
		{
			AttackTriggerSphere->SetActorHiddenInGame(false);
		}
	}
}

void AKH_CollisionBoxSword::OnTriggerShapeBeginOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& Hit)
{
	if(OtherActor != GetInstigator())
	{
		UGameplayStatics::ApplyDamage(OtherActor, DamageToDeal, GetInstigator()->GetController(), this, UDamageType::StaticClass());
	}
}

void AKH_CollisionBoxSword::CheckOverlappedActors(TArray<AActor*> OverlappedActors)
{
	for(AActor* OtherActor : OverlappedActors)
	{
		if(OtherActor != GetInstigator())
		{
			UGameplayStatics::ApplyDamage(OtherActor, DamageToDeal, GetInstigator()->GetController(), this, UDamageType::StaticClass());
		}
	}
}