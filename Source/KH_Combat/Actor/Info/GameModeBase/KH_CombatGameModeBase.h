// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "KH_CombatGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class KH_COMBAT_API AKH_CombatGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
