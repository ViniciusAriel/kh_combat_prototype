// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "KH_GameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class KH_COMBAT_API AKH_GameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
