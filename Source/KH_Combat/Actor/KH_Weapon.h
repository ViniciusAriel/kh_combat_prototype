// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "KH_Weapon.generated.h"

UCLASS()
class KH_COMBAT_API AKH_Weapon : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AKH_Weapon();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

private:
	// UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Weapon Properties", meta=(AllowPrivateAccess="true"))
	// enum EWeaponTypes WeaponType;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Combat Settings", meta=(AllowPrivateAccess="true"))
	class UBoxComponent* BoxCollisionComponent;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Visuals", meta=(AllowPrivateAccess="true"))
	class UStaticMeshComponent* StaticMeshComponent;

public:
	int32 ComboLenght;
	int32 ChangeComboLenght(int32 ComboModifier);

	int32 CurrentComboIndex = 0;
	virtual void WeaponAttack();

protected:
	virtual int32 IncrementCurrentComboIndex();

	void ResetComboIndex();

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Combat Settings", meta=(AllowPrivateAccess="true"))
	int32 BaseComboLenght;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Combat Settings", meta=(AllowPrivateAccess="true"))
	float BaseDamage = 10.f;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Combat Settings", meta=(AllowPrivateAccess="true"))
	float FinishingBlowDamageMultiplier = 1.5f;

	float DamageToDeal;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Combat Settings", meta=(AllowPrivateAccess="true"))
	class UDataTable* AttackInfoDataTable;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Combat Settings", meta=(AllowPrivateAccess="true"))
	TArray<FName> AttackInfoOrderedRowNames;

	FTimerHandle ComboResetTimerHandle;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Combat Settings", meta=(ALocation="true"))
	float ComboResetTimer = 0.8f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Debug", meta=(AllowPrivateAccess="true"))
	bool bDebugMode = true;
};
