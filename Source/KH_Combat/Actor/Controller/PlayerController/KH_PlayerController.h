// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "KH_PlayerController.generated.h"

/**
 * 
 */
UCLASS()
class KH_COMBAT_API AKH_PlayerController : public APlayerController
{
	GENERATED_BODY()
	
protected:
	virtual void BeginPlay() override;
	virtual void SetupInputComponent() override;

private:
	class AKH_MainCharacter* KH_ControlledCharacter;

private:
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Camera Settings", meta=(AllowPrivateAccess="true"))
	float LookUpTurnRateMultiplier = 1.f;
	void LookUp(float AxisValue);

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Camera Settings", meta=(AllowPrivateAccess="true"))
	float LookRightTurnRateMultiplier = 1.f;
	void LookRight(float AxisValue);

	void MovePawnForward(float AxisValue);
	void MovePawnRight(float AxisValue);

	void MakePawnJump();

	void MakePawnAttack();
};
