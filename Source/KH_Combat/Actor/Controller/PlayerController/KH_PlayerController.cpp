// Fill out your copyright notice in the Description page of Project Settings.


#include "KH_PlayerController.h"
#include "../../Pawn/Character/KH_Character/KH_MainCharacter.h"

void AKH_PlayerController::BeginPlay()
{
	Super::BeginPlay();

	KH_ControlledCharacter = Cast<AKH_MainCharacter>(GetPawn());

	if(!KH_ControlledCharacter)
	{
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, TEXT("KH_ControlledCharacter is null!!"), true, FVector2D(2.f, 2.f));
	}
}

void AKH_PlayerController::SetupInputComponent()
{
	Super::SetupInputComponent();

	if(InputComponent)
	{
		InputComponent->BindAxis(TEXT("MoveForward"), this, &AKH_PlayerController::MovePawnForward);
		InputComponent->BindAxis(TEXT("MoveRight"), this, &AKH_PlayerController::MovePawnRight);

		InputComponent->BindAxis(TEXT("LookUp"), this, &AKH_PlayerController::LookUp);
		InputComponent->BindAxis(TEXT("LookRight"), this, &AKH_PlayerController::LookRight);

		InputComponent->BindAction(TEXT("Jump"), EInputEvent::IE_Pressed, this, &AKH_PlayerController::MakePawnJump);

		InputComponent->BindAction(TEXT("Attack"), EInputEvent::IE_Pressed, this, &AKH_PlayerController::MakePawnAttack);
	}
}

void AKH_PlayerController::LookUp(float AxisValue)
{
	AddPitchInput(AxisValue * LookUpTurnRateMultiplier);
}

void AKH_PlayerController::LookRight(float AxisValue)
{
	AddYawInput(AxisValue * LookRightTurnRateMultiplier);
}

void AKH_PlayerController::MovePawnForward(float AxisValue)
{
	if(KH_ControlledCharacter)
	{
		KH_ControlledCharacter->MoveForward(AxisValue);
	}
}

void AKH_PlayerController::MovePawnRight(float AxisValue)
{
	if(KH_ControlledCharacter)
	{
		KH_ControlledCharacter->MoveRight(AxisValue);
	}
}

void AKH_PlayerController::MakePawnJump()
{
	if(KH_ControlledCharacter)
	{
		KH_ControlledCharacter->Jump();
	}
}

void AKH_PlayerController::MakePawnAttack()
{
	KH_ControlledCharacter->Attack();
}
