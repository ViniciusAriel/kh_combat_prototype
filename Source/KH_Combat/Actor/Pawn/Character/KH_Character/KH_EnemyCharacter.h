// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "../KH_Character.h"
#include "KH_EnemyCharacter.generated.h"

/**
 * 
 */
UCLASS()
class KH_COMBAT_API AKH_EnemyCharacter : public AKH_Character
{
	GENERATED_BODY()

public:
	AKH_EnemyCharacter();

private:
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Combat", meta=(AllowPrivateAccess="true"))
	class UDamageManagerComponent* DamageManagerComponent;

public:
	virtual float TakeDamage(float DamageAmount, struct FDamageEvent const& DamageEvent, class AController* EventInstigator, AActor* DamageCauser) override;
};
