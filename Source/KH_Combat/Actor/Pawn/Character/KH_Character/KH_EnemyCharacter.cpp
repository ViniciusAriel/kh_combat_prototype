// Fill out your copyright notice in the Description page of Project Settings.


#include "KH_EnemyCharacter.h"
#include "../../../../ActorComponent/DamageManagerComponent.h"

AKH_EnemyCharacter::AKH_EnemyCharacter()
{
	DamageManagerComponent = CreateDefaultSubobject<UDamageManagerComponent>(TEXT("Damage Manager Component"));
	AddInstanceComponent(DamageManagerComponent);
}

float AKH_EnemyCharacter::TakeDamage(float DamageAmount, struct FDamageEvent const& DamageEvent, class AController* EventInstigator, AActor* DamageCauser)
{
	Super::TakeDamage(DamageAmount, DamageEvent, EventInstigator, DamageCauser);

	if(DamageManagerComponent)
	{
		DamageManagerComponent->ChangeHealth(-DamageAmount);
	}

	return DamageAmount;
}

