// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "../KH_Character.h"
#include "KH_MainCharacter.generated.h"

/**
 * 
 */
UCLASS()
class KH_COMBAT_API AKH_MainCharacter : public AKH_Character
{
	GENERATED_BODY()

public:
	AKH_MainCharacter();

protected:
	virtual void BeginPlay() override;

private:
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Player Point of View", meta=(AllowPrivateAccess = "true"))
	class UCameraComponent* CameraComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Player Point of View", meta=(AllowPrivateAccess = "true"))
	class USpringArmComponent* SpringArmComponent;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Combat", meta=(AllowPrivateAccess="true"))
	class UDamageManagerComponent* DamageManagerComponent;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Weapon", meta=(AllowPrivateAccess="true"))
	FName WeaponSocket_R;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Weapon", meta=(AllowPrivateAccess="true"))
	FName WeaponSocket_L;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Weapon", meta=(AllowPrivateAccess="true"))
	class AKH_Weapon* EquippedWeapon;

public:
	UFUNCTION(BlueprintCallable)
	void EquipWeapon(TSubclassOf<class AKH_Weapon> WeaponToEquipClass);

	UFUNCTION(BlueprintCallable)
	void UnequipWeapon();

	virtual void Attack() override;

	virtual float TakeDamage(float DamageAmount, struct FDamageEvent const& DamageEvent, class AController* EventInstigator, AActor* DamageCauser) override;
};
