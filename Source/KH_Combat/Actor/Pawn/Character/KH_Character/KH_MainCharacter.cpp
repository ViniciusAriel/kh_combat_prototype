// Fill out your copyright notice in the Description page of Project Settings.


#include "KH_MainCharacter.h"
#include "Camera/CameraComponent.h"
#include "GameFramework/SpringArmComponent.h"
#include "../../../KH_Weapon.h"
#include "../../../../ActorComponent/DamageManagerComponent.h"


AKH_MainCharacter::AKH_MainCharacter()
{
	SpringArmComponent = CreateDefaultSubobject<USpringArmComponent>(TEXT("Spring Arm Component"));
	SpringArmComponent->SetupAttachment(GetMesh());
	CameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("Camera Component"));
	CameraComponent->SetupAttachment(SpringArmComponent);

	DamageManagerComponent = CreateDefaultSubobject<UDamageManagerComponent>(TEXT("Damage Manager Component"));
	AddInstanceComponent(DamageManagerComponent);
}

void AKH_MainCharacter::BeginPlay()
{
	Super::BeginPlay();
}

void AKH_MainCharacter::EquipWeapon(TSubclassOf<AKH_Weapon> WeaponToEquipClass)
{
	if(WeaponToEquipClass && !WeaponSocket_R.IsNone())
	{
		EquippedWeapon = GetWorld()->SpawnActor<AKH_Weapon>(WeaponToEquipClass);

		EquippedWeapon->AttachToComponent(GetMesh(), FAttachmentTransformRules::KeepRelativeTransform, WeaponSocket_R);
		EquippedWeapon->SetOwner(this);
		EquippedWeapon->SetInstigator(this);
	}
}

void AKH_MainCharacter::UnequipWeapon()
{
	if(EquippedWeapon)
	{
		EquippedWeapon->Destroy();
		EquippedWeapon = nullptr;
	}
}

void AKH_MainCharacter::Attack()
{
	Super::Attack();
	
	if(EquippedWeapon)
	{
		EquippedWeapon->WeaponAttack();
	}
}

float AKH_MainCharacter::TakeDamage(float DamageAmount, struct FDamageEvent const& DamageEvent, class AController* EventInstigator, AActor* DamageCauser)
{
	Super::TakeDamage(DamageAmount, DamageEvent, EventInstigator, DamageCauser);

	if(DamageManagerComponent)
	{
		DamageManagerComponent->ChangeHealth(-DamageAmount);
	}

	return DamageAmount;
}

