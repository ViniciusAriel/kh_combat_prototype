// Fill out your copyright notice in the Description page of Project Settings.


#include "KH_Character.h"

// Sets default values
AKH_Character::AKH_Character()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AKH_Character::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AKH_Character::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void AKH_Character::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

void AKH_Character::MoveForward(float MovementMultiplier)
{	
	AddMovementInput(GetActorForwardVector(), MovementMultiplier);
}

void AKH_Character::MoveRight(float MovementMultiplier)
{
	AddMovementInput(GetActorRightVector(), MovementMultiplier);
}

void AKH_Character::Attack()
{
	
}

